import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux'
import './index.css';
import App from './App';
import counterReducer from './store/reducers/counter'
import resultsReducer from './store/reducers/result'

import registerServiceWorker from './registerServiceWorker';

const logger = store => {
    return next => {
        return action => {
            const result = next( action );
            return result;
        }
    }
}
const rootReducer = combineReducers( {
    counter: counterReducer,
    results: resultsReducer
} )
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore( rootReducer, composeEnhancers( applyMiddleware( logger ) ) )
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById( 'root' ) );
registerServiceWorker();
