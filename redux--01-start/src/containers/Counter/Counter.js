import React, { Component } from 'react';
import { connect } from 'react-redux'
import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';
import * as actionTypes from '../../store/actions/index'

class Counter extends Component {



    render() {
        let resultList = this.props.results
            .map( ( result ) => {
                return <li key={result.id} onClick={() => this.props.onDeleteResult( result.id )}>
                    {result.value}
                </li>
            } )
        return (
            <div>
                <CounterOutput value={this.props.counter} />
                <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                <CounterControl label="Decrement" clicked={this.props.onDecrementCounter} />
                <CounterControl label="Add 5" clicked={this.props.onAddCounter} />
                <CounterControl label="Subtract 5" clicked={this.props.onSubtractCounter} />
                <hr />
                <button onClick={() => this.props.onStoreResult( this.props.counter )}>Store result</button>
                <strong>Results:</strong>
                <ul>
                    {resultList}
                </ul>
            </div >
        );
    }
}


const mapStateToProps = state => {
    return {
        counter: state.counter.counter,
        results: state.results.results,
    };
}

const mapDispatchToProps = dispatch => {
    return {
        onIncrementCounter: () => dispatch( actionTypes.incrementCounter() ),
        onDecrementCounter: () => dispatch( actionTypes.decrementCounter() ),
        onAddCounter: () => dispatch( actionTypes.addCounter( 5 ) ),
        onSubtractCounter: () => dispatch( actionTypes.subtractCounter( 5 ) ),
        onStoreResult: ( value ) => dispatch( actionTypes.storeResult( value ) ),
        onDeleteResult: ( idDelete ) => dispatch( actionTypes.deleteResult( idDelete ) )

    }
}

export default connect( mapStateToProps, mapDispatchToProps )( Counter );