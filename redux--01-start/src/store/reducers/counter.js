import * as  actionTypes from '../actions/actions';

const initialState = {
    counter: 0,
}

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.INCREMENT_COUNTER:
            return {
                counter: state.counter + 1,
            }
        case actionTypes.ADD_COUNTER:
            return {
                counter: state.counter + action.value,
            }
        case actionTypes.SUBTRACT_COUNTER:
            return {
                counter: state.counter - action.value,
            }
        case actionTypes.DECREMENT_COUNTER:
            return {
                counter: state.counter - 1,
            }
        default:
            return state;
    }

}

export default reducer;