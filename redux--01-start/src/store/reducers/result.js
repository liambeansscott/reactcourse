import * as  actionTypes from '../actions/actions';

const initialState = {
    results: [],
    idGen: 0,
}

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.DELETE_RESULT:
            return {
                results: [...state.results].filter(
                    result => result.id !== action.id
                )
            }
        case actionTypes.STORE_RESULT:
            return {
                results: [
                    ...state.results,
                    {
                        value: action.value,
                        id: state.idGen,
                    }
                ],
                idGen: state.idGen + 1,
            }
        default:
            return state;
    }

}

export default reducer;