export {
    incrementCounter,
    decrementCounter,
    addCounter,
    subtractCounter
} from './counter'

export {
    storeResult,
    deleteResult
} from './results'