import * as actionTypes from './actions'
export const storeResult = ( value ) => {
    return {
        type: actionTypes.STORE_RESULT,
        value: value,
    }
}
export const deleteResult = ( value ) => {
    return {
        type: actionTypes.DELETE_RESULT,
        value: value,
    }
}