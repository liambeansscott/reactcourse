import React, { Component } from 'react';

class Course extends Component {
    state= {
        title: null,
        id: null,
    }
    componentDidMount(){
        this.setState({
            id: this.props.match.params.courseId
        })
    }
    render () {
            const searchParams = new URLSearchParams(this.props.location.search).entries();
            for(let params of searchParams){
                if(params[1] && this.state.title !== params[1]){
                    console.log(params[1]);
                    this.setState({title: params[1]})
                }
            }
            
        return (
            <div>
                <h1>_COURSE_TITLE_</h1>
        <p>You selected course {this.state.title} with ID:{this.state.id}</p>
            </div>
        );
    }
}

export default Course;