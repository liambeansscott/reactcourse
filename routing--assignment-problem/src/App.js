import React, { Component } from 'react';
import {BrowserRouter, NavLink, Route, Switch} from 'react-router-dom'
import Courses from './containers/Courses/Courses';
import Users from './containers/Users/Users';
import RouteError from './containers/RouteError'
import classes from './App.module.css'

class App extends Component {
  render () {
    const ROUTES = {
      USERS: '/users',
      COURSES: '/courses',
    }
    return (
    <BrowserRouter>
      <div className={classes.App}>
        <ol style={{textAlign: 'left'}}>
          <li>Add Routes to load "Users" and "Courses" on different pages (by entering a URL, without Links)</li>
          <li>Add a simple navigation with two links => One leading to "Users", one leading to "Courses"</li>
          <li>Make the courses in "Courses" clickable by adding a link and load the "Course" component in the place of "Courses" (without passing any data for now)</li>
          <li>Pass the course ID to the "Course" page and output it there</li>
          <li>Pass the course title to the "Course" page - pass it as a param or score bonus points by passing it as query params (you need to manually parse them though!)</li>
          <li>Load the "Course" component as a nested component of "Courses"</li>
          <li>Add a 404 error page and render it for any unknown routes</li>
          <li>Redirect requests to /all-courses to /courses (=> Your "Courses" page)</li>
        </ol>
        <nav className={classes.nav}>
          <NavLink to={ROUTES.USERS}>Users</NavLink>
          <NavLink to={ROUTES.COURSES}>Courses</NavLink>
        </nav>
        <Switch>
          <Route path={ROUTES.USERS} component={Users}/>
          <Route path={ROUTES.COURSES} component={Courses}/>
          <Route path='/' component={RouteError}/>
        </Switch>
      </div>
    </BrowserRouter>
    );
  }
}

export default App;
