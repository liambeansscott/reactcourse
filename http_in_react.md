# Http in react
In a multi page application http works in a similar way, we send http request and we get back a new html page

in a single page react app it works differently, our single page app is strongly decoupled from the backend but they still need to communicate. They do this by sending and receiving JSON data. Our server is then typically a RESTful API, some endpoints we send http requests to.
