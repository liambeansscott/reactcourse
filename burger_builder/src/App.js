import React, { Component } from 'react';
import Layout from './components/Layout/Layout'
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder'
import Checkout from './containers/Checkout/Checkout'
import Orders from './containers/Orders/Orders'
import { BrowserRouter, Route, Switch, Redirect, withRouter } from 'react-router-dom'
import Auth from '../src/containers/Auth/Auth'
import Logout from '../src/containers/Auth/Logout/Logout'
import { connect } from 'react-redux'
import * as actions from '../src/store/actions/index'
class App extends Component {
  componentDidMount() {
    this.props.onTryAutoSignUp()
  }
  render() {
    let routes = (
      <Switch>
        <Route path="/burger-builder" component={BurgerBuilder} />
        <Route path="/auth" component={Auth} />
        <Redirect from="/" to="/burger-builder" />
      </Switch>

    );
    if ( this.props.isAuthenticated ) {
      let routes = (
        <Switch>
          <Route path="/logout" component={Logout} />
          <Route path="/orders" component={Orders} />
          <Route path="/checkout" component={Checkout} />
          <Route path="/burger-builder" component={BurgerBuilder} />
          <Redirect from="/" to="/burger-builder" />
        </Switch>


      );

    }
    return (
      <BrowserRouter>
        <div>
          <Layout>
            {routes}
            {/* <Switch>
              <Route path="/auth" component={Auth} />
              <Route path="/logout" component={Logout} />
              <Route path="/orders" component={Orders} />
              <Route path="/checkout" component={Checkout} />
              <Route path="/burger-builder" component={BurgerBuilder} />
              <Redirect from="/" to="/burger-builder" />
            </Switch> */}
          </Layout>
        </div>
      </BrowserRouter>
    );
  }
}
const mapsStateToProps = ( state ) => {
  return {
    isAuthenticated: state.auth.token !== null,
  }
}
const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignUp: () => dispatch( actions.authCheckState() )
  }
}
// export default WithErrorHandler(App, axios);
export default connect( mapsStateToProps, mapDispatchToProps )( App );

