import Burger from '../../Burger/Burger'
import Button from '../../UI/Button/Button'
import React from 'react'
import classes from './CheckoutSummary.module.css'

const checkoutSummary = (props) => {
    return (
        <div className={classes.checkoutSummary}>
            <Burger ingredients={props.ingredients} />
            <Button btnType="Danger" clicked={() => props.onCheckoutCancel()}>Cancel</Button>
            <Button btnType="Success" clicked={() => props.onCheckoutContinue()}> Continue </Button>
        </div>
    )
}

export default checkoutSummary;