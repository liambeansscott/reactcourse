import classes from './Order.module.css'
import React from 'react'
const order = ( props ) => {

    return (
        <div className={classes.Order}>
            Ingredients:
            <ul>{
                Object.keys( props.ingredients ).map(
                    ingredient => {
                        let { ingredients } = props;
                        return ingredients[ingredient] > 0
                            ? < li > {ingredient} {props.ingredients[ingredient]} </li>
                            : null
                    } )
            }</ul>
            <p>Price:
                <strong>£{parseFloat( props.price ).toFixed( 2 )}</strong>
            </p>
        </div >
    )

}

export default order;