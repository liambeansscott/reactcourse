import React, { Component } from 'react'
import { connect } from 'react-redux'
import Aux from '../hoc/Aux'
import classes from './Layout.module.css'
import Toolbar from '../Navigation/Toolbar/Toolbar'
import Sidedrawer from '../Navigation/Sidedrawer/Sidedrawer'

class Layout extends Component {
    state = {
        showSidedrawer: false,
    }
    sideDrawerClosedHandler = () => {
        this.setState(
            { showSidedrawer: false }
        )
    }

    sideDrawerOpenedHandler = () => {
        this.setState(
            { showSidedrawer: true }
        )
    }
    render() {
        return (
            <Aux>
                <Toolbar toggleSideBar={this.sideDrawerOpenedHandler}
                    isAuth={this.props.isAuthenticated} />
                <Sidedrawer
                    isAuth={this.props.isAuthenticated}
                    open={this.state.showSidedrawer}
                    close={this.sideDrawerClosedHandler} />
                <main className={classes.content}>
                    {this.props.children}
                </main>
            </Aux>
        );
    }
}
const mapStateToProps = ( state ) => {
    return {
        isAuthenticated: state.auth.token != null,
    }
}

export default connect( mapStateToProps )( Layout );