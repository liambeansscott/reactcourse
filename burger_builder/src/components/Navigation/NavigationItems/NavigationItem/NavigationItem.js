import React from 'react';
import { Link } from 'react-router-dom';
import classes from './NavigationItem.module.css'

const navigationItem = (props) => (
    <li className={classes.NavigationItem}>
        <Link to={props.linkInfo.linkPath}>
            {props.linkInfo.linkLabel}
        </Link>
    </li>
)

export default navigationItem;