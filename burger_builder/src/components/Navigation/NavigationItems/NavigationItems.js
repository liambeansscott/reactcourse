import React from 'react'
import NavigationItem from "./NavigationItem/NavigationItem";
import classes from './NavigationItems.module.css'

const ROUTES = {
    BURGER_BUILDER: {
        linkLabel: "Burger Builder",
        linkPath: '/burger-builder',
    },
    ORDERS: {
        linkLabel: "Orders",
        linkPath: "/orders",
    },
    AUTH: {
        linkLabel: "Auth",
        linkPath: "/auth",
    },
    LOGOUT: {
        linkLabel: "Log out",
        linkPath: "/logout",
    }
}
const navigationItems = ( props ) => (
    <ul className={classes.NavigationItems}>
        <NavigationItem linkInfo={ROUTES.BURGER_BUILDER} />
        {props.isAuthenticated ? <NavigationItem linkInfo={ROUTES.ORDERS} /> : null}
        {!props.isAuthenticated ? <NavigationItem linkInfo={ROUTES.AUTH} /> : <NavigationItem linkInfo={ROUTES.LOGOUT} />}

    </ul>
)

export default navigationItems;