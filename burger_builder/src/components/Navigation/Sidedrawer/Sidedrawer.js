import React from 'react';
import classes from './Sidedrawer.module.css'
import NavigationItems from '../NavigationItems/NavigationItems';
import Logo from '../../Logo/Logo'
import Backdrop from '../../UI/Backdrop/Backdrop'
import Aux from '../../hoc/Aux'
const sideDrawer = ( props ) => {

    let attachedClasses = props.open ? [classes.Sidedrawer, classes.Open] : [classes.Sidedrawer, classes.Close]
    return (
        <Aux>
            <Backdrop show={props.open} clicked={props.close} />
            <div className={attachedClasses.join( ' ' )}>
                <div className={classes.Logo}>
                    <Logo />
                </div>
                <NavigationItems isAuthenticated={props.isAuth} />
            </div>
        </Aux>


    )
}

export default sideDrawer;