import React from 'react'
import Logo from '../../Logo/Logo'
import classes from './Toolbar.module.css'
import NavigationItems from '../NavigationItems/NavigationItems'
import DrawerToggle from '../Sidedrawer/DrawerToggle/DrawerToggle'

const toolbar = ( props ) => (
    <header className={classes.Toolbar}>
        <DrawerToggle toggle={props.toggleSideBar} />
        <div className={classes.Logo}>
            <Logo />
        </div>
        <div className={classes.DesktopOnly}>
            <NavigationItems isAuthenticated={props.isAuth} />
        </div>
    </header>

)
export default toolbar