import React from 'react';
import logoImage from '../../../src/assets/images/logo-burger.png'
import classes from './Logo.module.css'
const logo = (props) => (
    <div className={classes.Logo}>
        <img src={logoImage} alt={"BurgerLogo"}/>
    </div>
)

export default logo;