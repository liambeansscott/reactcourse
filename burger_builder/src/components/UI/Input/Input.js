import React from 'react';
import classes from './Input.module.css'


const input = ( props ) => {
    let inputClass = props.valid
        ? classes.InputElement
        : classes.InvalidInputElement

    let inputElement = null;
    switch ( props.elementType ) {
        case ( 'input' ):
            inputElement = <input
                className={inputClass}
                onChange={props.changed}
                {...props.elementConfig}
            />
            break;
        case ( 'textArea' ):
            inputElement = <textarea
                className={inputClass}
                onChange={props.changed}
                {...props.elementConfig} />
            break;
        default:
            inputElement = <input
                className={inputClass}
                onChange={props.changed}
                {...props.elementConfig} />

    }
    return (
        <div className={classes.Input}>
            <label>
                {props.elementLabel}
            </label>
            {inputElement}
        </div>

    )
}

export default input;