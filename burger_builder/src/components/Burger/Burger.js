import React from 'react'
import BurgerIngredient from './BurgerIngredient/BurgerIngredient'
import classes from './Burger.module.css'

const burger = (props) => {
    // make array of the ingredients
    const ingredientsTransformed = Object.keys(props.ingredients)
        //for each ingredient present, make an array in size equal to the number of that ingredient
        .map(ingredientKey => {
            // map through each element of the array created, returning a burger ingredient
            return [...Array(props.ingredients[ingredientKey])]
                .map((_, i) => {
                    return <BurgerIngredient key={ingredientKey + i} type={ingredientKey} />
                })
        }).reduce((arr, el) => {
            return (arr.concat(el))
        }, [])

    let ingredients = ingredientsTransformed.length > 0 ? ingredientsTransformed : <p>Please add some ingredients to your burger</p>
    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {ingredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    );
}

export default burger;