import React from 'react'
import Aux from '../../hoc/Aux'
import Button from '../../UI/Button/Button'

const orderSummary = (props) => {
const ingredientSummary = Object.keys(props.ingredients)
    .map((ingredient,i) => <ul key={i}><span>{ingredient}</span>: {props.ingredients[ingredient]}</ul>)
    return(
        <Aux>
            <h3>Your Order:</h3>
            <p>Ingredients:</p>
            {ingredientSummary}
            <strong>£{props.totalPrice.toFixed(2)}</strong>
            <p>Continue to checkout</p>
            <Button btnType={"Danger"} clicked={props.purchaseCancel}>Cancel</Button>
            <Button btnType={"Success"} clicked={props.purchaseContinue}>Continue</Button>

        </Aux>
    )
}

export default orderSummary;