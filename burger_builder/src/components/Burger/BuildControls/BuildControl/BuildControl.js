import React from 'react'
import classes from './BuildControl.module.css'


const BuildControl = (props) => {

    return(    
        <div className={classes.BuildControl}>
            <div className={classes.Label}>{props.control.label}</div>
            <button className={classes.More} onClick={()=>props.addIngredient(props.control.control)}>+</button>

            <button 
                className={classes.Less} 
                onClick={() => props.removeIngredient(props.control.control)}
                disabled={props.disabled}>-</button>
        </div>
    );
}

export default BuildControl;