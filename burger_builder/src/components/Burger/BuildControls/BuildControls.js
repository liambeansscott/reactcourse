import React from 'react'
import classes from './BuildControls.module.css'
import BuildControl from './BuildControl/BuildControl'

const buildControls = ( props ) => {
    const controlLabels = [
        { label: "Salad", control: "salad" },
        { label: "Meat", control: "meat" },
        { label: "Bacon", control: "bacon" },
        { label: "Cheese", control: "cheese" },
    ]
    const controls = controlLabels.map( control => <BuildControl
        addIngredient={props.addIngredient}
        removeIngredient={props.removeIngredient}
        key={control.label} control={control} disabled={props.disable[control.control]}
    /> )

    return (
        <div className={classes.BuildControls}>
            <p>Total Price: <strong>£{props.price.toFixed( 2 )}</strong></p>
            {controls}
            <button onClick={props.toggleModal} className={classes.OrderButton} disabled={!props.canPurchase}>
                {props.isAuth ? " ORDER NOW"
                    : "SIGN UP TO ORDER"}

            </button>
        </div>
    )
}

export default buildControls;