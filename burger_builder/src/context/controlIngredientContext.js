import React from 'react';

const controlIngredientContext = React.createContext({
    addIngredient: () => {},
    removeIngredient: () => {}
})

export default controlIngredientContext;