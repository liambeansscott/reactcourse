import React, { Component } from 'react';
import Input from '../../components/UI/Input/Input'
import Button from '.././../components/UI/Button/Button'
import classes from './Auth.module.css'
import * as actions from '../../store/actions/index';
import { connect } from 'react-redux';
import Spinner from '../../components/UI/Spinner/Spinner';
import { Redirect } from 'react-router';

class Auth extends Component {
    state = {
        controls: {
            email: {
                elementLabel: "Email",
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'e-mal'

                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true,
                },
                valid: false,
                touched: false,

            },
            password: {
                elementLabel: "Password",

                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'password'

                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6,
                },
                valid: false,
                touched: false,

            },
        },
        isSignUp: true,
    };
    componentDidMount() {
        if ( !this.props.buildingBurger && this.props.authRedirectPath !== '/checkout' ) { this.props.onSetAuthRedirectPath() }


    }
    submitHandler = ( event ) => {
        event.preventDefault();
        this.props.onAuth( this.state.controls.email.value, this.state.controls.password.value, this.state.isSignUp )
    }
    inputIsValid = ( value, rules ) => {
        let valid = true
        if ( rules.required ) {
            valid = value.trim() !== ''
        }
        if ( rules.minLength !== undefined ) {
            valid = value.trim().length >= rules.minLength
        }
        return valid
    }

    inputChangedHandler = ( event, controlName ) => {
        const { value } = event.target
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.inputIsValid( event.target.value, this.state.controls[controlName].validation ),
                touched: true,
            }
        }
        this.setState( { controls: updatedControls } )
    }

    switchAuthModeHandler = () => {
        this.setState( prevState => {
            return { isSignUp: !prevState.isSignUp }
        } )
    }
    render() {

        const formElementsArray = [];
        for ( let key in this.state.controls ) {
            formElementsArray.push( {
                id: key,
                config: this.state.controls[key]
            } )
        }

        let form = formElementsArray.map( formElement => (

            <Input
                key={formElement.id}
                elementLabel={formElement.config.elementLabel}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                valid={formElement.config.valid}
                changed={( event ) => this.inputChangedHandler( event, formElement.id )}


            />
        ) )

        if ( this.props.loading ) {
            console.log( 'loading' )
            form = <Spinner />
        }
        let authRedirect = null
        if ( this.props.isAuthenticated ) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }
        return (
            <div className={classes.auth}>
                {authRedirect}
                <form onSubmit={( event ) => this.submitHandler( event )}>
                    {form}
                    <Button btnType="Success">Submit</Button>
                </form>
                <Button
                    clicked={this.switchAuthModeHandler}
                    btnType="Danger"
                >Switch to {!this.state.isSignUp ? 'SIGNUP' : 'SIGNIN'}</Button>
            </div>
        )
    }
}

const mapStateToProps = ( state ) => {
    return {
        loading: state.auth.loading,
        isAuthenticated: state.auth.token !== null,
        buildingBurger: state.burgerBuilder.building,
        authRedirectPath: state.auth.authRedirect,

    }
}
const mapDispatchToProps = ( dispatch ) => {
    return {
        onAuth: ( email, password, isSignUp ) => dispatch( actions.auth( email, password, isSignUp ) ),
        onSetAuthRedirectPath: () => dispatch( actions.setAuthRedirectPath( '/' ) ),
    }
}

export default connect( mapStateToProps, mapDispatchToProps )( Auth );