import React, { Component } from 'react'
import Button from '../../../components/UI/Button/Button'
import Spinner from '../../../components/UI/Spinner/Spinner'
import Input from '../../../components/UI/Input/Input'
import classes from './ContactData.module.css'
import { connect } from 'react-redux'
import * as orderActions from '../../../store/actions/index'

class ContactData extends Component {
    state = {
        orderForm: {
            customer: {
                name: {
                    elementLabel: "Name",
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'your name'

                    },
                    value: '',
                    validation: {
                        required: true,
                        minLength: 5,
                    },
                    valid: true,
                },
                address: {
                    elementLabel: "E-mail",
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'your address'
                    },
                    value: '',
                    validation: {
                        required: false,
                    },
                    valid: true,

                },
                email: {
                    elementLabel: "Address",
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'your email'
                    },
                    value: '',
                    validation: {
                        required: false,
                    },
                    valid: true,
                },
            }
        },
    }

    orderHandler = ( event ) => {
        event.preventDefault();
        const formData = { ...this.state.orderForm.customer }

        let orderObject = {
            price: this.props.price,
            ingredients: this.props.ingredients,
        }
        for ( let key of Object.keys( formData ) ) {
            orderObject = { ...orderObject, [key]: formData[key].value }
        }
        this.props.onOrderBurger( this.props.token, orderObject )
    }

    inputChangedHandler = ( event, inputIdentifier ) => {
        const { value } = event.target
        const orderFormUpdated = { ...this.state.orderForm.customer }
        const updatedFormElement = { ...orderFormUpdated[inputIdentifier] }
        updatedFormElement.value = value
        updatedFormElement.valid = this.inputIsValid( updatedFormElement.value, updatedFormElement.validation )
        orderFormUpdated[inputIdentifier] = updatedFormElement
        this.setState( {
            orderForm:
                { customer: orderFormUpdated, }
        } )
    }

    inputIsValid = ( value, rules ) => {
        let valid = true
        if ( rules.required ) {
            valid = value.trim() !== ''
        }
        if ( rules.minLength !== undefined ) {
            valid = value.trim().length >= rules.minLength
        }
        return valid
    }

    formIsValid = ( form ) => {
        let valid = true;
        Object.keys( form ).map(
            inputElement => {
                if ( form[inputElement].valid === false ) {
                    valid = false
                }
            }
        )
        return valid;
    }
    render() {
        let { customer } = this.state.orderForm;
        return (
            <div className={classes.ContactData}>
                <h4>Contact Form</h4>
                {this.props.loading
                    ? <Spinner />
                    : <form>
                        {

                            Object.keys( ( customer ) )
                                .map( ( input, i ) => {
                                    return <Input
                                        key={i}
                                        elementLabel={customer[input].elementLabel}
                                        elementType={customer[input].elementType}
                                        elementConfig={customer[input].elementConfig}
                                        value={customer[input].value}
                                        valid={customer[input].valid}
                                        changed={( event ) => this.inputChangedHandler( event, input )}
                                    />
                                } )}
                        <Button btnType="Success" clicked={( event ) => this.orderHandler( event )}>ORDER</Button>
                    </form>
                }

            </div>

        )
    }
}
const mapStateToProps = ( state ) => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        loading: state.orders.loading,
        token: state.auth.token
    }
}



const mapDispatchToProps = ( dispatch ) => {
    return {
        onOrderBurger: ( token, orderObject ) => dispatch( orderActions.purchaseBurger( token, orderObject ) ),
    }
}
export default connect( mapStateToProps, mapDispatchToProps )( ContactData );