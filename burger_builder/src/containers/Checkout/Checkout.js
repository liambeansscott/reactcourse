import React, { Component } from 'react';
import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary'
import { Route, Redirect } from 'react-router-dom'
import ContactData from './ContactData/ContactData';
import { connect } from 'react-redux'

class Checkout extends Component {


    checkoutCancel = () => this.props.history.replace( '/burger-builder' );
    checkoutContinue = () => this.props.history.replace( this.props.match.path + '/contact-data' )

    render() {

        let summary = <Redirect to="/" />
        if ( this.props.ingredients ) {
            const purchasedRedirect = this.props.purchased ? <Redirect to="/" /> : null;
            summary = <div>
                {purchasedRedirect}
                <CheckoutSummary
                    ingredients={this.props.ingredients}
                    onCheckoutCancel={this.checkoutCancel}
                    onCheckoutContinue={this.checkoutContinue}
                />
                <Route
                    path={this.props.match.path + '/contact-data'}
                    render={( props ) =>
                        <ContactData {...props} />}
                />
            </div>
        }


        return (
            summary
        )
    }
}

const mapStateToProps = ( state ) => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        purchased: state.orders.purchased
    }
}


export default connect( mapStateToProps )( Checkout );