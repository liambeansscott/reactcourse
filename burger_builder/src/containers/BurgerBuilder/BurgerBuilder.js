import React, { Component } from 'react'
import Aux from '../../components/hoc/Aux'
import Burger from '../../components/Burger/Burger'
import BuildControls from '../../components/Burger/BuildControls/BuildControls'
import Modal from '../../components/UI/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary'
import Spinner from '../../components/UI/Spinner/Spinner'
import { connect } from 'react-redux'
import * as burgerBuilderActions from '../../store/actions/index'


class BurgerBuilder extends Component {

    state = {
        canPurchase: false,
        purchasing: false,
        loading: false,
    }

    componentDidMount() {
        this.props.onInitIngredients()
    }
    addIngredientHandler = ( type ) => {
        this.props.onIngredientAdd( type );
    }

    removeIngredientHandler = ( type ) => {
        this.props.onIngredientRemove( type );
    }

    updatePurchaseState = () => {
        const ingredients = { ...this.props.ingredients }
        const sum = Object.keys( ingredients )
            .map( key => {
                return ingredients[key]
            } )
            .reduce( ( sum, el ) => {
                return sum + el
            }, 0 );
        return sum > 0;
    }

    purchasingHandler = () => {
        if ( this.props.isAuthenticated ) {
            this.setState( { purchasing: true } )
        } else {
            this.props.onSetAuthRedirectPath( '/checkout' );
            this.props.history.push( '/auth' )
        }

    }

    purchasingCancelHandler = () => {
        this.setState( { purchasing: false } )
    }

    purchasingContinueHandler = () => {
        this.props.onInitPurchase()
        this.props.history.push( {
            pathname: '/checkout'
        } )
    }

    render() {
        const disabledInfo = { ...this.props.ingredients }
        for ( let key in disabledInfo ) {
            disabledInfo[key] = disabledInfo[key] < 1
        }

        let burger = !this.props.error && this.props.ingredients ?
            <Aux>
                <Burger ingredients={this.props.ingredients} />
                <BuildControls
                    isAuth={this.props.isAuthenticated}
                    toggleModal={this.purchasingHandler}
                    canPurchase={this.updatePurchaseState()}
                    price={this.props.totalPrice}
                    disable={disabledInfo}
                    addIngredient={this.addIngredientHandler}
                    removeIngredient={this.removeIngredientHandler}
                />
            </Aux> :
            <Spinner />

        return (
            <Aux>
                {this.state.purchasing ?
                    <Modal show={this.state.purchasing} modalClosed={this.purchasingCancelHandler}>
                        {
                            this.state.loading ?
                                <Spinner />
                                : <OrderSummary
                                    ingredients={this.props.ingredients}
                                    purchaseCancel={this.purchasingCancelHandler}
                                    purchaseContinue={this.purchasingContinueHandler}
                                    totalPrice={this.props.totalPrice} />}

                    </Modal>
                    : null}

                {burger}
            </Aux>
        );
    }
}

const mapStateToProps = ( state ) => {
    return {
        ingredients: state.burgerBuilder.ingredients,
        totalPrice: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        isAuthenticated: state.auth.token !== null,
    }

}
const mapDispatchToProps = ( dispatch ) => {
    return {
        onIngredientAdd: ( ingredient ) => dispatch( burgerBuilderActions.addIngredient( ingredient ) ),
        onIngredientRemove: ( ingredient ) => dispatch( burgerBuilderActions.removeIngredient( ingredient ) ),
        onInitIngredients: () => dispatch( burgerBuilderActions.initialiseIngredients() ),
        onInitPurchase: () => dispatch( burgerBuilderActions.purchaseInit() ),
        onSetAuthRedirectPath: ( path ) => dispatch( burgerBuilderActions.setAuthRedirectPath( path ) )

    }
}
export default connect( mapStateToProps, mapDispatchToProps )( BurgerBuilder );