import * as  actions from '../actions/actionTypes';

const initialState = {
    totalPrice: 4,
    error: '',
    building: false,
};

const INGREDIENT_PRICES = {
    salad: 0.5,
    cheese: 0.3,
    meat: 1.3,
    bacon: 0.7
}

const reducer = ( state = initialState, action ) => {
    console.log( 'action', action )
    switch ( action.type ) {
        case actions.ADD_INGREDIENT:
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.value]: state.ingredients[action.value] + 1,
                    building: true,

                },
                totalPrice: state.totalPrice + INGREDIENT_PRICES[action.value]
            };
        case actions.REMOVE_INGREDIENT:
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.value]: state.ingredients[action.value] - 1,
                    building: true,
                },
                totalPrice: state.totalPrice - INGREDIENT_PRICES[action.value]

            };
        case actions.SET_INGREDIENTS:
            return {
                ...state,
                ingredients: action.value,
                totalPrice: initialState.totalPrice,
                error: false,
                building: false,
            };

        case actions.SET_ERROR:
            return {
                ...state,
                error: action.value,
            }
        default:
            return state;
    }
}

export default reducer;