import * as actionTypes from './actionTypes'
import axios from '../../axios-orders'

export const addIngredient = ( ingredient ) => { return { type: actionTypes.ADD_INGREDIENT, value: ingredient } }
export const removeIngredient = ( ingredient ) => { return { type: actionTypes.REMOVE_INGREDIENT, value: ingredient } }
export const initialiseIngredients = () => {
    return dispatch => {
        axios.get( 'https://burger-builder-10d00.firebaseio.com/ingredients.json' )
            .then( response => {
                return dispatch( setIngredients( response.data ) )
            } )
            .catch( error => {
                return setError( error )
            } )
    }
}
export const setError = ( error ) => { return { type: actionTypes.SET_ERROR, value: error } }
export const setIngredients = ( ingredients ) => { return { type: actionTypes.SET_INGREDIENTS, value: ingredients } }
