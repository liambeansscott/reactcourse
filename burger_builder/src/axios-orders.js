import axios from 'axios'

const instance = axios.create({
        baseURL: 'https://burger-builder-10d00.firebaseio.com/'
    })

export default instance;