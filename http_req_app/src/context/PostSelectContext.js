import React from 'react';

const selectPostContext = React.createContext({
    selectPost: () => {}
})

export default selectPostContext;