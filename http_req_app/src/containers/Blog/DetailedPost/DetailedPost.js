import React, {Component} from 'react'
import classes from './DetailedPost.module.css'
import axios from 'axios';

class DetailedPost extends Component{
    state = {
        selectedPost: null
    }
    componentDidMount(){
        this.loadData()
    }

    componentDidUpdate(){
        this.loadData()
    }

    loadData(){
        if(this.props.match.params.id){
            if(!this.state.selectedPost || this.state.selectedPost.id !== +this.props.match.params.id){
                axios.get('https://jsonplaceholder.typicode.com/posts/' + this.props.match.params.id)
                    .then(response => this.setState({selectedPost: response.data}))
            }
        }
    }



    render(){
        let selectedPost = this.state.selectedPost

        let detailedPost = selectedPost ?     
        <div className={classes.DetailedPost}>
            <h1>{selectedPost.title}</h1>
            <p className={classes.Author}>Post Author</p>
            <p>{selectedPost.body}</p>
            <button>Delete</button>
        </div> :   
        <div className={classes.DetailedPost}>
            <p>Select a post</p>
        </div>

        return (detailedPost)
    }
}



export default DetailedPost