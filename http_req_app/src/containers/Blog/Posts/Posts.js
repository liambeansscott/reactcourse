import React, { Component } from 'react';
// import SelectPostContext from '../../../context/PostSelectContext';
import RecentPost from '../RecentPosts/RecentPost/RecentPost';
import DetailedPost from '../DetailedPost/DetailedPost'
import { Route } from 'react-router-dom';
import axios from 'axios';
import classes from './Posts.module.css';

class Posts extends Component{
    state = {
        posts:[],
    }

    componentDidMount(){
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then(response => this.setState({posts: response.data}))
            .catch(
                error => console.log(error)
                )
    }
    render(){
        const recentPosts = this.state.error ?
            <p>Somethinf went wrong bro</p> 
                : [...this.state.posts.slice(0,3)]
                    .map((post, i) =>
                        <Link to={this.props}
                            <RecentPost key={i} content={post.body} title={post.title} id={post.id} />
                    )
        return(
            <div>
                <div className={classes.Posts}>
                    {recentPosts}
                </div>
                <Route path="/:id" exact component={DetailedPost} /> 
            </div>
        );
    }
}

export default Posts;