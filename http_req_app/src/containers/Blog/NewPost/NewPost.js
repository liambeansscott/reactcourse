import React, {Component} from 'react'
import axios from 'axios'
import classes from './NewPost.module.css'
import {Redirect} from 'react-router-dom'

class NewPost extends Component {
    state = {
        title: '',
        content: '',
        author: 'Liam',
        submitted: false,
    }

    onSubmitHandler = () => {
        const post = {...this.state}
        axios.post('https://jsonplaceholder.typicode.com/posts', post)
            .then(response => {
                console.log(response)
                this.setState({submitted: true})
            })
    }

    render(){
        let redirect = this.state.submitted 
            ? <Redirect to="/"/>
            : null;
        return(
            <div className={classes.NewPost}>
                {redirect}
                <h1>new post</h1>
                
                <p>Title</p>
                <textarea className={classes.TitleInput} value={this.state.title} onChange={(event) => this.setState({title: event.target.value})}/>
        
        
                <p>Content</p>
                <textarea className={classes.ContentInput} value={this.state.content}onChange={(event) => this.setState({content: event.target.value})}/>
        
                <p>Author</p>
                <select  value={this.state.author} onChange={(event) => this.setState({author: event.target.value})}>
                    <option value='Liam'>Liam</option>
                    <option value='Felix666'>Felix666</option>
                </select>
                <button onClick={() => this.onSubmitHandler()}>Submit</button>
            </div>
        );
    }
}


export default NewPost;