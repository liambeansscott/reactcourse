import React from 'react'
import RecentPost from './RecentPost/RecentPost'
import classes from './RecentPosts.module.css';

const recentPosts = (props) => {
    const posts = props.posts
        .map((post, i) => <RecentPost key={i} content={post.body} title={post.title} id={post.id}/>)
    return(
        <div className={classes.RecentPosts}>
            {posts}
        </div>
    ) 
}

export default recentPosts;