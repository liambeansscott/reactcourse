import React from 'react'
import classes from './RecentPost.module.css'

const recentPost = (props) => 
            <div 
                className={classes.RecentPost} 
                onClick={()=> props.clicked(props.id)}>
                <h2>{props.title}</h2>
                <p>Author</p>
            </div>

export default recentPost;