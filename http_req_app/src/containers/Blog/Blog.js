import React, { Component } from 'react'
import classes from './Blog.module.css'
import Posts from './Posts/Posts'
import AsynchronousComponent from '../../components/HOC/AsynchronousComponent'
import { Route, NavLink, Switch } from 'react-router-dom'

const asynchronousNewPost = AsynchronousComponent(() => import('./NewPost/NewPost'))

class Blog extends Component{
    state={
        posts: [],
        selectedPostId: null,
        error: null
    }

    // Component will mount is bad place to fetch data, will not have returned by first render so there will be one instance of a component rendering WITHOUT DATA!
    // Componentwillmount is called twice, on server and by client, in case of serverside react app, inefficient

    //ComponentDidmount ensures app renders with data fine, then populates with data from server

    render(){


        return(
            <div className={classes.Blog}>
                <ul>
                    <li>
                        <NavLink exact to="/">Home</NavLink>
                    </li>
                    <li>
                        <NavLink exact to="/new-post">NewPost</NavLink>     
                    </li>
                </ul>
                <Switch>
                    <Route path="/new-post" exact component={asynchronousNewPost} />
                    <Route path="/" component={Posts} />
                </Switch>
            </div>
        )
    }
}

export default Blog