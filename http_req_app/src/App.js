import React from 'react';
import Blog from './containers/Blog/Blog';
import './App.css';
import {BrowserRouter} from 'react-router-dom'

function App() {
  return (
    <BrowserRouter>
          <Blog/>
    </BrowserRouter>
  )
}

export default App;
