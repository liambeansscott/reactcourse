# Routing
Most apps have more than one page, "posts" "users" "accounts"
So how do we provide this in a Single Page Application (SPA) with a single .html

# How we achieve this
Depending on the path we the user is on we rendere a different set of .jsx.  
We will do this by parsing the path url, we use a router package to do this as parsing this is non-trivial.

### Flow
Parse URL --> Read config --> Render appropriate JSX

### Using routing
we use react-router and react-router-dom.
We import BrowserRouter from react-router in App.js or Index.js an we wrap our container component with this HOC and then we can utilise routing capabilities
