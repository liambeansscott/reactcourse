import React, { useState } from 'react';
import './App.css';
import UserInput from './UserInput/userInput'
import UserOutput from './UserOutput/userOutput'

function App() {
  const [userName1, setUserName1] = useState(
    "bert"
  )

  const [userName2, setUserName2] = useState(
    "ernie"
  )

  const name1changedHandler = (event) => {
    setUserName1(
      event.target.value
    )
  }


  return (
    <div className="App">
        <UserInput name={userName1} changed={name1changedHandler}/>
        <UserOutput name={userName1}/>
        <UserOutput name={userName2}/>

    </div>
  );
}

export default App;
