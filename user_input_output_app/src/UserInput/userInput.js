import React from 'react';

const userInput = (props) => {
    return (
        <input value={props.name} onChange = {props.changed}></input>
    )
}

export default userInput;