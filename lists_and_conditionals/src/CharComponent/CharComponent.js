import React from 'react'

const CharComponent = (props) => {
    const style = {
        display: 'inline-block',
        textalign: 'center',
        margin: '16px',
        border: '1px solid black'

    }
    return(
    <p style={style} onClick={props.click}>{props.character}</p>
    );
}

export default CharComponent;