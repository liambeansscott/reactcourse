import React, {useState} from 'react';
import './App.css';
import ValidationComponent from './ValidationComponent/ValidationComponent'
import CharComponent from './CharComponent/CharComponent'

function App() {
  const [inputState, setInputState] = useState(
    { input: ""
    }  
  );
  const onInputChangeHandler = (event) => {
    setInputState({
      input: event.target.value,
    })
  }

  const deleteCharacter = index => {
    const updatedInputArray = [...inputState.input].filter((letter, i)=> i !== index)
    let updatedInputString = "";
    updatedInputArray.forEach(element => {
      updatedInputString+=element
    });
    setInputState(
      { ...inputState,
      input: updatedInputString 
      }
    )
  }
  const charComponents = inputState.input.split('').map( (letter,index) => 
    <CharComponent key={inputState.currentKey} character={letter} click={() => deleteCharacter(index)}/> 
    )

  return (
    <div className="App">
      <input value={inputState.input} onChange={ event => onInputChangeHandler(event)}></input>
      <h2>input length</h2>
      <p>{inputState.input.length}</p>
      {charComponents}
      <ValidationComponent minimum={5} inputLength={inputState.input.length}/>
      
    </div>
  );
}

export default App;
