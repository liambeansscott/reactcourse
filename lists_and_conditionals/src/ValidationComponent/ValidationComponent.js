import React from 'react'

const ValidationComponent = ({inputLength, minimum}) => {
    return inputLength >= minimum ?  <p/> : <p>Input too short</p>
}

export default ValidationComponent
