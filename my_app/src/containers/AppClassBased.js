import React, { Component } from 'react';
import './App.css';
import PersonsClassBased from '../components/Persons/PersonsClassBased'
import Cockpit from '../components/Cockpit/Cockpit';

class AppClassBased extends Component {

    constructor(props) {
        super(props)
        console.log('[App.js] constructor')

    }

    state= {
        persons:
        [
          { id: "1", name: "Liam", age: "24" },
          { id: "2", name: "Max", age: "18" },
          { id: "3", name: "Elena", age: "27" }
        ], 
      showPersons: true,
      showCockpit: true
      }

    static getDerivedStateFromProps(props, state){
        console.log('[App.js] getDerivedStatefromProps', props)
        return state;
    }

    componentDidMount(){
        console.log('[App.js] componentDidMount')
    }

    deletePersonHandler = (index) => {
        const persons = [...this.state.persons];
        persons.splice(index,1);
        this.setState({persons: persons})
    }


    nameChangedHandler = (event, id) => {  
        const updatedPersons = [...this.state.persons].map(
        person => person.id ===id ?
            person = {
            ...person,
            name: event.target.value,
            }
            : person
        )
        this.setState({persons: updatedPersons})
    }

    
    togglePersonsHandler = () => {
        this.setState({showPersons: !this.state.showPersons})
    }

    toggleCockpitHandler = () => {
        this.setState({showCockpit: !this.state.showCockpit})
    }


    render() {
        console.log('[App.js] render');
        let persons = null;
        let cockpit = null
        if(this.state.showPersons){
          persons = <div><PersonsClassBased persons={this.state.persons} click={this.deletePersonHandler} changed = {this.nameChangedHandler}/></div>
        }
        if(this.state.showCockpit){
            cockpit=<Cockpit persons= {this.state.persons} togglePersonsHandler= {this.togglePersonsHandler}/>
        }
        return (

            <div className="App">
            {/* <p className={classes.join(" ")}> this is working</p>
            <StyledButton showPersons={personsState.showPersons} onClick = {togglePersonsHandler}>toggle show users</StyledButton> */}
            <button onClick={this.toggleCockpitHandler}>toggleCockpit</button>
            {cockpit}
            {persons}
            </div>
        );
    }    
}

export default AppClassBased;