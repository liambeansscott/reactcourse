import React, { useState } from 'react';
import './App.css';
import Persons from '../components/Persons/Persons'
import Cockpit from '../components/Cockpit/Cockpit';
import AuthContext from '../context/auth-context'

const App = props => {


  const [personsState, setPersonsState] = useState(
    {
      persons:
      [
        { id: "1", name: "Liam", age: "24" },
        { id: "2", name: "Max", age: "18" },
        { id: "3", name: "Elena", age: "27" }
      ], 
    showPersons: true,
    isAuthenticated: false
    }
  )

  const loginHandler = () => {
    console.log('loginHandlercalled')
    console.log(personsState.isAuthenticated)
    setPersonsState((prevState, props) => {
      console.log(prevState.isAuthenticated)

      return{      
        ...personsState,
        isAuthenticated: !prevState.isAuthenticated
      }
    })
  }

  const deletePersonHandler = (index) => {
    const persons = [...personsState.persons]
    persons.splice(index,1);
    setPersonsState(
      { 
        ...personsState,
        persons: persons
      }
    )
  }

  const togglePersonsHandler = () => {
    setPersonsState((prevState, props) => {
      return{
        ...personsState,
        showPersons: !prevState.showPersons
      }

    })
  }

  const nameChangedHandler = (event, id) => {
    
    const updatedPersons = [...personsState.persons].map(
      person => person.id ===id ?
        person = {
          ...person,
           name: event.target.value,
        }: person
    )
    setPersonsState({
      ...personsState,
      persons: updatedPersons
    })
  }

  let persons = null;

  if (personsState.showPersons) {
    persons = 
      <div>
      <Persons 
        persons = {personsState.persons} 
        click = {deletePersonHandler} 
        changed = {nameChangedHandler}/>
      </div>

  }


  return (
      <div className= "App">
        {/* <p className={classes.join(" ")}> this is working</p>
        <StyledButton showPersons={personsState.showPersons} onClick = {togglePersonsHandler}>toggle show users</StyledButton> */}
        <AuthContext.Provider value={{authenticated: personsState.isAuthenticated, login: loginHandler}}>
          <Cockpit 
            persons= {personsState.persons} 
            togglePersonsHandler= {togglePersonsHandler}
          />
          {persons}
        </AuthContext.Provider>
      </div>
  );
}
export default App;
