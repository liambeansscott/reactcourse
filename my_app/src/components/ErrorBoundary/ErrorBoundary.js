import React from 'react';

const ErrorBoundary = (props) => {
    const [errorState, setErrorState] = useState({error: null})
    componentDidCatch = (error) => {
        setErrorState({error: error})
    }

return( 
    errorState.error ? <h1>something went wrong: {errorState.error}</h1> : <h1/>
    )    
}

export default ErrorBoundary