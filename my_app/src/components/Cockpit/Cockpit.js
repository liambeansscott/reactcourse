import React, {useEffect, useRef, useContext} from 'react'
import AuthContext from '../../context/auth-context'
// import styled from 'styled-components'

const Cockpit = (props) => {

    const toggleButtonRef = useRef(null);  
    const authContext = useContext(AuthContext)
    useEffect(() => {
        toggleButtonRef.current.click()
    }, [])

    useEffect(()=>{
        setTimeout( () => {
            alert('savedDatatoClout')
        }, 1000)
        console.log('[cockpit.js] useEffect')
    }, [props.persons.length])

    // const StyledButton = styled.button`
    //     background-color: ${props => props.showPersons ? 'red' : 'black'};
    //     color: white;
    //     font: inherit;
    //     border: 1px solid blue;
    //     padding: 8px;
    //     cursor: pointer; 
    //     &:hover {
    //     background-color: lightGreen;
    //     color: black;
    //     }
    //     `

    let classes = []
    if (props.persons.length >=1) {
        classes.push('red')
        if (props.persons.length >=2) {
            classes.push('bold')
        }
    }
    return(
        <div>
            <h1>Hi, I'm a react app!</h1>
            <p className={classes.join(" ")}> this is working</p>
            <button 
                ref={toggleButtonRef} 
                onClick = {props.togglePersonsHandler}>
                toggle show users
            </button>
            <button onClick = {authContext.login}>LogIn</button>
        </div>     
    );
}
export default React.memo(Cockpit);
