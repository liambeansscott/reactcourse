import * as React from 'react';
import PropTypes from 'prop-types'
import AuthContext from '../../../context/auth-context'
// import styled from 'styled-components'
const personDynamic = props => {

    console.log('[PersonDynamic.js] rendering...')
    // const StyledDiv = styled.div`
    //     border: 1px solid #eee;
    //     width: 60%;
    //     margin: auto;
    //     box-shadow: 0 2px 3px #ccc;
    //     text-align: center;
    //     @media (min-width: 500px) {  width: 450px } 
    // `
    return (
        <div>
            <p>I'm {props.name} I am  {props.age} </p>
            <button onClick={props.click}> delete user </button>
            <p>{props.children}</p>
            <input type="text" onChange={props.changed} value={props.name}></input>
            <AuthContext.Consumer>
                {(context) => context.authenticated ? <p>logged in</p> : <p>logged out</p>}
            </AuthContext.Consumer>
        </div>



    )
}

personDynamic.propTypes = {
    click: PropTypes.func,
    changed: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.string,
}
export default personDynamic;