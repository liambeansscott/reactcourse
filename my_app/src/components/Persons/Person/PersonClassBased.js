import React, { Component } from 'react';
import styled from 'styled-components'
class PersonClassBased extends Component {
    constructor(props){
        super(props)
        this.inputElementRef = React.createRef();
    }

    componentDidMount(){
        this.inputElementRef.current.focus()
    }
    render(){
        let StyledDiv = styled.div`
            border: 1px solid #eee;
            width: 60%;
            margin: auto;
            box-shadow: 0 2px 3px #ccc;
            text-align: center;
            @media (min-width: 500px) {  width: 450px } 
            `
        console.log('[PersonDynamic.js] rendering...')

        return(
                <StyledDiv>
                    <p>I'm {this.props.name} I am  {this.props.age} </p>
                    <button onClick={this.props.click}> delete user </button>
                    <p>{this.props.children}</p>
                    <input 
                        ref = {this.inputElementRef}
                        type="text" 
                        onChange={this.props.changed} 
                        value={this.props.name}>
                    </input>
                </StyledDiv>
            

        )
    }
}
export default PersonClassBased;