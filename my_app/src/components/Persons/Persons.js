import React from 'react'
import PersonDynamic from './Person/PersonDynamic'
const Persons = (props) =>{ 
    console.log('[Persons.js] rendering...')
    return(
        props.persons.map((person, index) => {
        return(
        <PersonDynamic 
            key = {person.id}
            name = {person.name}
            age = {person.age}  
            click = {() => props.click(index)}
            changed = {(event) => props.changed(event, person.id)}
        >
        </PersonDynamic>
        )
        })
    )
}
    


export default Persons;