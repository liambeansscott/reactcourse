import React, {Component} from 'react'
import PersonClassBased from './Person/PersonClassBased'
class PersonsClassBased extends Component {

    static getDerivedStateFromProps(state, props){
        console.log('[PersonsClassBased.js] getDerivedStateFrokmProps')
        return state
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log('[PersojnsClassBased.js] shouldComponentUpdate')
        // SomeLogic
        return this.props.persons!==nextProps.persons? true: false
    }

    getSnapshotBeforeUpdate(prevProps, prevState){
        console.log('[PersonsClassBased.js] getSnapshotBeforeUpdate')
    }
    
    componentDidUpdate(){
        console.log('[PersonsClassBased.js] componentDidUpdate')
    }

    render(){
        return this.props.persons.map((person,index) => {
            return(                
            <PersonClassBased
                key = {person.id}
                name = {person.name}
                age = {person.age}  
                click = {() => this.props.click(index)}
                changed = {(event) => this.props.changed(event, person.id)}
            >
            </PersonClassBased>
            )
        })
    }
}

export default PersonsClassBased;