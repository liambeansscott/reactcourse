# Working with lists and conditionals
### Rendering content conditionally
We used a ternary checking a boolean in a state to render or not render elements
We then moved this logic out of the JSX returned by the component and into a separate method in the component

### Outputting lists
Key: React components are pure JS we are just returning some syntax reading like html.
So, we use the JS to check our state and convert it to some readable JSX.
We map through the array of data in the state and map them to components using props
NOTE: We will get an error asking us to assign a unique key prop to each unique component in the array!

### Lists and State
We can edit the data stored in state directly. As lists of components render the data in the state visually we can update our UI by updating state

### updating state immutably
An issue with the method we've used (making a ref to the state and editing this), is that we are mutating some data, which can lead to unpredictable apps.
As such, we should make copies of data structures, edit these, and then REPLACE the old state with a new one.

### lists and keys
React has a DOM and DOMshell, what is on the screen and what would be on screen were the DOM rendered now.
In this way it only renders updated elements.
Keys help React identify which items have changed, are added, or are removed. Keys should be given to the elements inside the array to give the elements a stable identity.
##### Deep dive
The use-cases for lists are numerous and lists within an application can be performance heavy. Because performance is an important aspect, when you are using lists you need to make sure they are designed for optimal performance.
React reconciler keeps two copies of our array, the old one and the updated one, and compares each key value (the other props) pair to the old ones. Redrawing any that are different.

# Styling Rect components and elements

### Setting styles dynamically
Using inline styles we define a jso called style and pass it as a prop to component, we can then edit properties in the object based on conditionals to dynamically alter style.

### Setting class name dynamically
We can make some classes in a global .css file
e.g. .red {color: red}
then make a variable in our .js const classes = "red"
and pass it as a prop "className" to an element
e.g. ```<p className = {classes}> </p>```
We can make classes an array and push styles to it conditionally, allowing us to dynamically change the styling/class of an html element.
**Remember, everything here is just JS**

### Addig using radium
limitations of inline: we can't use pseudo selectors
*why?* 
we could use the .css file but then it isn't scoped to the component only, other buttons/elements with the same tag in our project would get the same styling
*this feels like not a problem bc we use custom elements all the time*

So, we going to install another package!
```
npm install --save radium
```

*we should learn about radium*

##### Radium higher order component
We now use
```javascript
export default Radium(App)
```
making a *higher order component*

### Using Radium for media queries

##### Aside Media Queries
we use these for responsive webpages
```css
@media (min-width: 500px) {
    .PersonDynamic { width: 450px }
}
```
we can use this formatting in our .css file.
BUT since we are looking at using Radium we should try add this styling into our .js

To include radium selectors we need to wrap our root div with 
```js
<StyleRoot>
    <div className="App">
    </div>
</StyleRoot>
```

### Styled Components

```bash
npm install --save tyled-components
```

is the library needed to utilise styled components
*Note: we are now goint to get rid of radium*

Now we style as:
```javascript
    const StyledDiv = styled.div`
        border: 1px solid #eee;
        width: 60%;
        margin: auto;
        box-shadow: 0 2px 3px #ccc;
        text-align: center;
        @media (min-width: 500px) {  width: 450px } 
    `
    return (
        <StyledDiv>
            <div/>
        </StyledDiv>
    )
```

Doing this we have our CSS and js in one file!
We can store our styled components in separate files and use them in multiple components

Whats nice about styled components is that we get to write in super normal CSS
```css
.div{
    background-color: white;
}
```
rather than jsx
```javascript
.div {
    backgroundColor:'White',
}
```

### Changing style dynamically with styled-components
cool:
bc we are using js template literal can check a condition using a ternary in our styled.element
e.g.
```javascript
<StyledButton alt= {someBoolean}>
...
const StyledButton = styled.button`
    color: ${props => props.alt ? 'red' : 'black' }
`
```

### CSS modules
We might not like the bloat of styled-components and its scoping


# Debugging react apps

### Finding logical errors
 Sometimes errors don't crash a program or stop it from running they simply cause unexpected behaviour.
 *Like we've given the computer instructions it can read and run, they are just not the instructions we meant to give*
 Note: All errors can be static (compile time), and caught at the time the code compiles, or dynamic (run time) which elude the interpreter, compiler and developer.

 ##### Using source map
 In dev tools and looking in the source map we can add break points at a function that is crashing, so we dont need to console log everything!
 (console logging everything is kinda trash)

 ### React developers tools
 A plugin for Chrome containing tools for react development (makes sense right?)
 Its pretty tight and also self explanatory

 ### Using error boundaries
 We wrap our component that throws error with error boundary components (this makes them higher order components)
 We handle the errors in here
 Note: We have to give the errorboundary component the key id.

 # Diving deeper into react components and internals
 ### A better project structure
 **What should be it's own component?**
* So look at PersonDynamic, it is pretty focussed it just outputs the information it is given, it doesn't need anymore splitting.
* App.js is doing alot, 
    * it manages state, 
    * it has a toggle button
    * it renders people and buttons
    * as it manages state it should have a LEAN render method
*This being the case we can make a Persons component, we can pass a list of Person to this and let it render them*
A nice new file structure is  

src
    |
    |_containers
    |    |
    |    |_App.js
    |    
    |   
    |_components
        |_Persons
                |  Persons.js
                |__Person
                    |
                    |_Person.js

This file structure is nice, it removes problems with naming conventions, all files are plural if needed (ending 's') and their singulars are in the same file (remember how long this was in jet!)


### Stateful and stateless components
We like minimum stateful components as these are the only places we modify state, in this way we are trying to restrict the points at which we modify data.
A stateless/dumb/presentational component we write, it works when we give it the right data, so it shoulod ALWAYS work.

BOTH stateless and stateful components can access STATE
ONLY stateful components can access lifecycle hooks


### Component life cycle
ONLY AVAILABLE IN CLASS BASED COMPONENTS


**constructor**
The constructor() method is called before anything else, when the component is initiated, and it is the natural place to set up the initial state and other initial values. we call it as
```javascript
constructor(props){
    super(props)
    state={...}
}
```

**getDerivedStateFromProps**
The getDerivedStateFromProps() method is called right before rendering the element(s) in the DOM.
This is the natural place to set the state object based on the initial props.
It takes state as an argument, and returns an object with changes to the state.
called as:
```javascript
  static getDerivedStateFromProps(props, state) {
    return {stateProperty: props.favcol };
  }
```

**render**
The render() method is required, and is the method that actual outputs HTML to the DOM.
(I think we know this good by now)

**componentDidMount**
The componentDidMount() method is called after the component is rendered.
This is where you run statements that requires that the component is already placed in the DOM!



### Component LifeCycle- Update
for when a class based component (w state) has props passed to it (which update state)
getDerivedStateFromProps(props,state), synch state to props
shouldComponentUpdate(nextProps, nextState), decide to continue or not
render()
**getDerivedStateFromProps**
Also at updates the getDerivedStateFromProps method is called. This is the first method that is called when a component gets updated.
This is still the natural place to set the state object based on the initial props.
**shouldComponentUpdate**
shouldComponentUpdate
In the shouldComponentUpdate() method you can return a Boolean value that specifies whether React should continue with the rendering or not
The default value is true.
In the beliow example should component update is false, this means that getDerivedState will get the state from the original props and will override the colour change.
If componentShouldUpdate is true, then a new state will be set
```javascript
shouldComponentUpdate() {
    return false;
  }
  changeColor = () => {
    this.setState({favoritecolor: "blue"});
  }
  render() {
    return (
      <div>
      <h1>My Favorite Color is {this.state.favoritecolor}</h1>
      <button type="button" onClick={this.changeColor}>Change color</button>
      </div>
    );
  }
```
```javascript
```

##### Static keyword
```static``` is a keyword that makes a function call relate to a class rather than INSTANCES of the class 

### Using useEffect() in functional components
useEffect is a react hook, it is a function that will be called everytime render is called.
Everytime we edit a name, app.js is rendered, so cockpit is rendered, so useEffect is called.
it performs the same function of it is componentDidMount and componentDidUpdate in one
We dont need getDerivedState because we are passing props down, so we can useState in conjunction with props to cover this. 

##### Controlling useEffect()
useEffect() is useful but currently runs every render cycle, what if we only want to call our function in useEffect when a given prop is updated?
Well, we can use the second argument of use effect! We can pass an array of any variables in the component, useEffect will then be called after every update of this variable. 
The following method is called 1 second after every instance in which props.persons is updated (this includes delete and update!)
```javascript
useEffect(()=>{setTimeout(alert('data saved to cloud'),1000)} , [props.persons])
```

if we pass an empty array, the effect will only run once on start up and then never again, as the effect is not dependent on changes!

### Cleanup with LifeCycle Hooks and react hooks
We can use componentWillUnmount() to run any code that needs to be run when a component is removed
we can use useEffect() as follows
```javascript
useEffect(  
    ()=>{setTimeout(alert('data saved to cloud'),1000)
    return ()=>{}
    } 
    , [props.persons])
```
with our return () => {} being run after each render cycle

### Using shouldComponentUpdate to optimise
**React does not deeply compare props or state**
**When props or state are updated React assumes we need to render**
**this is not always necessary**
when the component update lifecycle begins shouldComponentUpdate() is called and triggers the component to render (when it returns true).
By using conditional logic to decide whether the component should rerender we can make efficiency savings.
e.g.
right now, shouldComponentUpdate returns true in App.js, so App is rerendered, so Cockpit and Persons is rerendered, so Person is rerendered. This is not necessary or efficient.
Also when we remove cockpit, we change the state of App (showCockpit) triggering App.js to render, triggering persons etc.
THIS IS SUPER USEFUL!
```javascript

    shouldComponentUpdate(nextProps, nextState){
        console.log('[PersojnsClassBased.js] shouldComponentUpdate')
        // SomeLogic
        return this.props.persons!==nextProps.persons? true: false
    }
```

Note: this is why we have to update the state using copies and resetting the state, ratehr than modifying the current state.
>> BECAUSE we are only comparing the pointers in our comparison rather than the elements of the array. If we modify the existing state in memory then the reference doesn't change and so the comparison will always be false

### Optimizing functional components with React.memo()
We use React.memo() to wrap the component, react will store a snapshot of the component, the component will only re render if the inputs for the component update.
So we should wrap components that dont need to render with every render of the parent component with React.memo()

**Always remember that optimisation checks, like shouldComponentUpdate and React.memo() are code that runs, and does have impacts on performance**

### How React renders to the dom
render() does not necessarily mean the dom is updated. 
Lets say we miss a shouldComponentUpdate() condition, it isn't necessarily re drawn.
React has a virtual dom in JS, it compares the old virtual and the new re rendered dom (this is made when render is called)
React makes a comparison it compares the old virtual dom for the new one
If it can detect differences, it reaches out to the new dom and renders it **in the places where the changes were found**

the real dom is only touched if there are differences, this is bc accessing the real dom is hella slow, so we are trying to  prevent this happening where is possible.

### Rendering adjacent JSX elements
Consider that in Person we are returning an array of adjacent elements. React allows this provided we hav ekeys for each element.
So we can do
```javascript
const component = (props) => {
    return (
        [<p key='1'/>,<h1 key='2'/>,<div key='3'/>]
    )
}
```
here we've lost our styling, but it is worth seeing this 

### Another method
we can make a higher order component (HOC) with no styling to serve as a wrapper
```javascript
const aux = props => props.children
...
...
return <aux><CustomReactElement/><CustomReactElement2></aux>
``` 

allowing us to return two adjacent react elements with no styling in their wrapper!

### React.Fragment
```javascript

return <React.Fragment><CustomReactElement/><CustomReactElement2></React.Fragment>
``` 

### Higher Order Components: Introduction
We use things like 
```javascript
<div className={class}></div>
```

Which is fine, but there are other options
e.g. a custom component

```javascript
const withClass = props => {
    <div className={props.classes}>
        {props.children}
    </div>
}

...
...
<WithClass classes={classes.App}>HTML</WithClass>
```
Or as follows, in this case we need to make it clear that we AREN'T making a component, we write a function that returns a component
```javascript
const withClass = (WrappedComponent, className) => {
    return props => (
        <div className={className}>
            <WrappedComponent>
        </div>
    )
}
```

we use it as follows in our component we are wrapping

```javascript
export default withClass(App, classes.App)
```

Rule of thumb: If a HOC is mainly for styling, we should include it in teh jsx code. If it has logic we should wrap the export as above.

### Passing unknown props
In the above example if we wrap Persons with our HOC the Person component will not have it's data. This is because it outputs Persons after wrapping it with another div, there are no props set.
But in the with class function props ARE the props we want, they are just not assigned.

```javascript
const withClass = (WrappedComponent, className) => {
    return props => (
        <div className={className}>
            <WrappedComponent {...props}>
        </div>
    )
}
```
The spread operator pulls all of our props out of our props object and spreads them as new key value pairs in our wrapped components.
In this way we can dynamically pass our props to our wrapped components.

### Setting state correctly
When the state of a component is called it doesnt necessarily trigger a render like you would expect. React instead schedules the update and render synchronously and will perform it when it has available resources.
So, updating state as
```javascript
this.setState({
    someProperty: this.state.someProperty++
})
```
is problematic because there is no guarantee that ```this.state.someProperty``` is the most recent value of the state that you are expecting, like it might not have been updated yet.
So if you are updating state in a away that depends on the old state, you can use another syntax

```javascript
this.setState((prevState, props)=>{
    return{ someProperty: prevState.someProperty++ }
})
```
Using this method React guarantees you that what you are accesing is the old state.

### PropTypes

```
npm install --save prop-types
```

Note: running the --save option means that your package dependencies will be updated

Note: running with TS would probably help

### Using Refs
Say we want to access a JSX element, but we don't want to use two way binding. Like: When we load all our Person we want to give our final Person focus.
This is hard! like do we use component did mount? do we use jquery? >> This would only select the first instance of an 'input' found on the document
So we use the ```ref``` key word as a prop
```javascript
<input ref={(inputEl)=>{this.inputElement=inputEl}}/> 
```

where the argument is a reference to the element we put it on. We now have a global variable representing this input element

we can then use the ```componentDidMount()``` lifecycle mount to access the variable after the variable has been created:

```javascript
componentDidMount(){
    this.inputElement.focus()
}
```

Alternate:
```javascript
constructor(){
    this.inputElementRef = React.createRef()
}
...
<input ref={this.inputElementRef}/>
```
Note: This is for class based

### Using Refs with useRef() with React hooks
```javascript
const toggleButtonRef = useRef(null);  

useEffect(()=>{
    toggleButtonRef.current.click()},[]
);

<button ref={toggleButtonRef}/>
```
This takes some unpacking: we call use Ref and create a ref to this component to toggleButtonRef, we then assign this ref to button. 
Note: We need to use useEffect(,[]) with to make sure click is called after the ref is assigned!

### Prop chain problems
So sometimes we have a variable we want to pass as props from a component A to a component D, which is the child of a component C and C of B, B being a child of A. 
This means we have to pass the variable from A, to B, to C, to D, even if B and C do not need it.



### Context API
so we make a HOC context with the property as a state, we then wrap any components we want to use the property with ```<AuthContext.Provider>``` then in that component we wrap the elements using the property with ```<AuthContext.Consumer>```

We can also utilise the useContext() react hook. We keep the ```<AuthContext.Provider>``` wrapper but can now just write
```javascript
import AuthContext from 'context/auth-context'
const authContext = useContext(AuthContext)

authContext.Property
```
much nicer!
